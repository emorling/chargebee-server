const express = require('express')
const chargebee = require("chargebee")
// CORS is enabled only for demo. Please dont use this in production unless you know about CORS
const cors = require('cors')

chargebee.configure({site : "ideahuntws-test", 
  api_key : "test_OiqBUU1WNSGbjpbcdYbZbSJLXvcdiooEdv"});
const app = express()

app.use(express.urlencoded())
app.use(cors())



// https://apidocs.chargebee.com/docs/api/customers#create_a_customer
// https://apidocs.chargebee.com/docs/api/subscriptions#create_subscription_for_customer
app.post("/api/init_customer", (req, res, next) => {
  const customerData = {
    id: req.body.id,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email, 
    billing_address: {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
    },
  };

  // setting default plan to free
  const subscriptionData = {
    id: req.body.subscription_id,
    plan_id: 'free',
  };

  getCreateUser(customerData)
    .then((result) => {
      getCreateSubscription(customerData.id, subscriptionData)
        .then((result) => {
          res.send(result);
        })
        .catch((error) => {
          console.log('getCreateSubscription error', error);
          res.status(error.http_status_code).send(error.message);
        });
    })
    .catch((error) => {
      console.log('getCreateUser error', error);
      res.status(error.http_status_code).send(error.message);
    });
});

function getCreateUser(data) {
  return new Promise((resolve, reject) => {
    chargebee.customer.retrieve(data.id).request(
    function(error, result){
      if(error){
        chargebee.customer.create(data).request(function(error, result){
          if(error){
            reject(error);
          } else {
            resolve(result.customer);
          }
        });
      } else {
        resolve(result.customer);
      }
    });
  });
}

function getCreateSubscription(customerId, data) {
  return new Promise((resolve, reject) => {
    chargebee.subscription.retrieve(data.id).request(
    function(error, result) {
      if(error){
        chargebee.subscription.create_for_customer(customerId, data).request(function(error, result){
          if(error) {
            reject(error);
          } else {
            resolve(result.subscription);
          }
        });
      } else {
        resolve(result.subscription);
      }
    });
  });
}


// subscription_id -- currently setting subscription_id to the same as customer_id
// plan_id - e.g. 'free'
app.post("/api/generate_checkout_existing_url", (req, res) => {
  chargebee.hosted_page.checkout_existing({
    subscription : {
      id: req.body.subscription_id,
      plan_id : req.body.plan_id,
    }, 
  }).request(function(error,result){
    if(error){
      //handle error
      console.log(error);
    }else{
      res.send(result.hosted_page);
    }
  });
});

// id - the customer id
app.post("/api/generate_portal_session", (req, res) => {
  chargebee.portal_session.create({
    customer : {
      id: req.body.id,
    }, 
  }).request(function(error,result){
    if(error){
      //handle error
      console.log(error);
    }else{
      res.send(result.portal_session);
    }
  });
});



app.get('/', (req, res) => res.send('Hello World!'))

app.listen(8000, () => console.log('Example app listening on port 8000!'))
